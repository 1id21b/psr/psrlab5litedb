﻿
using Lab5LiteDB.Model;
using LiteDB;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class TrailerRep
    {
        private LiteDatabase db { get; set; }
        public TrailerRep(LiteDatabase _db)
        {
            db = _db;
        }

        public void AddTrailer(Trailer _d)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            col.Insert(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteTrailer(string id)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            try
            {
                col.Delete(int.Parse(id));
                Console.WriteLine("Usunięto naczepe o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiej naczepy");
            }
        }
        public void EditTrailer(string id, string name)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            try
            {
                Trailer t = col.Find(dr => dr.id == int.Parse(id)).First();
                t.name = name;
                col.Update(t);
                Console.WriteLine("Zedytowano naczepe o Id: " + id);
                Console.WriteLine(t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma naczepy z takim id");
            }
        }
        public void ShowTrailers()
        {
            var col = db.GetCollection<Trailer>("Trailer");
            var trailers = col.FindAll();
            foreach (Trailer t in trailers)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowTrailer(string id)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            Trailer _t = new Trailer();
            try
            {
                _t = col.Find(d => d.id == int.Parse(id)).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma naczepe z takim id");
            }
        }

    }
}
