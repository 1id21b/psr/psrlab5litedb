﻿using Lab5LiteDB.Model;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class InitializeDb
    {
        LiteDatabase db;
        public InitializeDb(LiteDatabase _db)
        {
            db = _db;
        }

        public void Initialize()
        {
            List<Trailer> trailers = new List<Trailer>();
            List<Truck> trucks = new List<Truck>();
            List<Driver> drivers = new List<Driver>();
            Trailer przyczepa1 = new Trailer("Przyczepa1");
            Trailer przyczepa2 = new Trailer("Przyczepa2");
            Trailer przyczepa3 = new Trailer("Przyczepa3");
            Trailer przyczepa4 = new Trailer("Przyczepa4");

            trailers.AddRange(new Collection<Trailer>{przyczepa1, przyczepa2,przyczepa3, przyczepa4 });

            

            

            var col = db.GetCollection<Trailer>("Trailer");
            if(col.FindAll().Count() == 0)
            {
                Console.WriteLine("Inicjalizuje baze danych");
                col.InsertBulk(trailers);

                Truck truck1 = new Truck("Ciezarowka1", przyczepa1.id);
                Truck truck2 = new Truck("Ciezarowka1", przyczepa3.id);
                Truck truck3 = new Truck("Ciezarowka1", przyczepa2.id);

                trucks.AddRange(new Collection<Truck> { truck1, truck2, truck3 });

                var col2 = db.GetCollection<Truck>("Truck");
                col2.InsertBulk(trucks);

                Driver driver1 = new Driver("Jan", "Kowalski", "12345678901", 25, truck1.id);
                Driver driver2 = new Driver("Stefan", "Nowak", "38271328183", 27, truck2.id);
                Driver driver3 = new Driver("Adam", "Adam", "83218093281", 43, truck3.id);
                Driver driver4 = new Driver("Norbert", "Pik", "48328493028", 27, truck1.id);

                drivers.AddRange(new Collection<Driver> { driver1, driver2, driver3, driver4 });

                var col3 = db.GetCollection<Driver>("Driver");
                col3.InsertBulk(drivers);
                Console.WriteLine("Baza została zainicjalizowana");
            }

        }

    }
}
