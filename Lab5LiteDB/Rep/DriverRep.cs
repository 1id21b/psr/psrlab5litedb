﻿using Lab5LiteDB.Model;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5LiteDB.Rep
{
    class DriverRep
    {

        LiteDatabase db;
        public DriverRep(LiteDatabase _db)
        {
            db = _db;
        }

        public void AddDriver(Driver _d)
        {
            var col = db.GetCollection<Driver>("Driver");
            col.Insert(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteDriver(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            try
            {
                col.Delete(int.Parse(id));
                Console.WriteLine("Usunięto kierowce o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiego kierowcy");
            }
        }
        public void EditDriver(string id, string name, string surname, string pesel, string age)
        {
            var col = db.GetCollection<Driver>("Driver");
            try
            {
                Driver d = col.Find(dr => dr.id == int.Parse(id)).First();
                d.name = name;
                d.surname = surname;
                d.pesel = pesel;
                d.age = int.Parse(age);
                col.Update(d);
                Console.WriteLine("Zedytowano kierowce o Id: " + id);
                Console.WriteLine(d);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma kierowcy z takim id");
            }
        }
        public void ShowDrivers()
        {
            var col = db.GetCollection<Driver>("Driver");
            var Drivers = col.FindAll();
            foreach (Driver t in Drivers)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowDriver(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            Driver _t = new Driver();
            try
            {
                _t = col.Find(d => d.id == int.Parse(id)).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma kierowcy z takim id");
            }
        }
    }
}
