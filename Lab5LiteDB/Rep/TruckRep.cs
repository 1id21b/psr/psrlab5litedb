﻿
using Lab5LiteDB.Model;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class TruckRep
    {
        LiteDatabase db;
        public TruckRep(LiteDatabase _db)
        {
            db = _db;
        }

        public void AddTruck(Truck _d)
        {
            var col = db.GetCollection<Truck>("Truck");
            col.Insert(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteTruck(string id)
        {
            var col = db.GetCollection<Truck>("Truck");
            try
            {
                col.Delete(int.Parse(id));
                Console.WriteLine("Usunięto ciezarowke o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiej ciezarowki");
            }
        }
        public void EditTruck(string id, string name)
        {
            var col = db.GetCollection<Truck>("Truck");
            Truck _t = new Truck();
            try
            {
                _t = col.Find(dr => dr.id == int.Parse(id)).First();
                _t.name = name;
                col.Update(_t);
                Console.WriteLine("Zedytowano ciezarowke o Id: " + id);
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }
        public void ShowTrucks()
        {
            var col = db.GetCollection<Truck>("Truck");
            var Trucks = col.FindAll();
            foreach (Truck t in Trucks)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowTruck(string id)
        {
            var col = db.GetCollection<Truck>("Truck");
            Truck _t = new Truck();
            try
            {
                _t = col.Find(d => d.id == int.Parse(id)).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }

        public void ShowDriversOfTruck(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            var drivers = col.Find(d =>d.truck_Id == int.Parse(id)).ToList();
            if (drivers.Count > 0)
                Console.WriteLine("Do ciężrówki o id: " + id + " przypisani są:");
            else
                Console.WriteLine("Bledne id ciezarowki albo nie ma zadnych kierowcow przypisanych");
            foreach (Driver d in drivers)
            {
                Console.WriteLine(d);
            }
        }
    }
}
