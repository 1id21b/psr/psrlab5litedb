﻿using Lab5LiteDB.Model;
using Lab5LiteDB.Rep;
using Lab5MongoDb.Rep;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5LiteDB
{
    class Program
    {
        static void Main(string[] args)
        {

            var database = new LiteDatabase(@"PSRLiteDB.db");
            InitializeDb init = new InitializeDb(database);
            init.Initialize();
            string menu = "";
            while (menu != "q")
            {
                MainMenu();
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        DriverMenu(database);
                        continue;
                    case "2":
                        TruckMenu(database);
                        continue;
                    case "3":
                        TrailerMenu(database);
                        continue;
                }
            }

        }

        static void MainMenu()
        {
            Console.WriteLine("Menu glowne:");
            Console.WriteLine("1. Menu Kierowcy");
            Console.WriteLine("2. Menu Ciezarowki");
            Console.WriteLine("3. Menu Naczepy");
            Console.WriteLine("q. Wyjście");
        }
        static void DriverMenu(LiteDatabase db)
        {
            DriverRep driverR = new DriverRep(db);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu kierowcy:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl");
                Console.WriteLine("3. Wyswietl pojedyńczy");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Driver d = new Driver();
                        Console.WriteLine("Podaj Imie");
                        d.name = Console.ReadLine();
                        Console.WriteLine("Podaj Nazwisko");
                        d.surname = Console.ReadLine();
                        Console.WriteLine("Podaj Pesel");
                        d.pesel = Console.ReadLine();
                        Console.WriteLine("Podaj Wiek");
                        d.age = int.Parse(Console.ReadLine());
                        Console.WriteLine("Podaj Id ciezarowki");
                        try { d.truck_Id = int.Parse(Console.ReadLine()); } catch (Exception) { d.truck_Id = 0; }
                        driverR.AddDriver(d);
                        continue;
                    case "2":
                        driverR.ShowDrivers();
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id kierowcy");
                        string id = Console.ReadLine();
                        driverR.ShowDriver(id);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id kierowcy");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj nowe imie kierowcy");
                        string name = Console.ReadLine();
                        Console.WriteLine("Podaj Nazwisko");
                        string surname = Console.ReadLine();
                        Console.WriteLine("Podaj Pesel");
                        string pesel = Console.ReadLine();
                        Console.WriteLine("Podaj Wiek");
                        string age = Console.ReadLine();
                        driverR.EditDriver(id, name, surname, pesel, age);
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id kierowcy");
                        id = Console.ReadLine();
                        driverR.DeleteDriver(id);
                        continue;
                }
            }
        }
        static void TruckMenu(LiteDatabase db)
        {
            TruckRep truckR = new TruckRep(db);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu ciezarowki:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl");
                Console.WriteLine("3. Wyswietl pojedyńczy");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("6. Pokaż kierowcow przypisanych do ciezarowki");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Truck t = new Truck();
                        Console.WriteLine("Podaj nazwe ciezarowki");
                        t.name = Console.ReadLine();
                        Console.WriteLine("Podaj Id naczepy");
                        t.trailer_Id = int.Parse(Console.ReadLine());
                        truckR.AddTruck(t);
                        continue;
                    case "2":
                        truckR.ShowTrucks();
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id ciezarowki");
                        string id = Console.ReadLine();
                        truckR.ShowTruck(id);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id ciezarowki");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj nowa nazwe ciezarowki");
                        truckR.EditTruck(id, Console.ReadLine());
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id ciezarowki");
                        id = Console.ReadLine();
                        truckR.DeleteTruck(id);
                        continue;
                    case "6":
                        Console.WriteLine("Podaj id ciezarowki");
                        id = Console.ReadLine();
                        truckR.ShowDriversOfTruck(id);
                        continue;
                }
            }
        }
        static void TrailerMenu(LiteDatabase db)
        {
            TrailerRep trailerR = new TrailerRep(db);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu naczepy:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl wszystkie");
                Console.WriteLine("3. Wyswietl pojedyńczy");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Trailer t = new Trailer();
                        Console.WriteLine("Podaj nazwe naczepy");
                        t.name = Console.ReadLine();
                        trailerR.AddTrailer(t);
                        continue;
                    case "2":
                        trailerR.ShowTrailers();
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id naczepy");
                        string id = Console.ReadLine();
                        trailerR.ShowTrailer(id);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id naczepy");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj nowa nazwe naczepy");
                        trailerR.EditTrailer(id, Console.ReadLine());
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id naczepy");
                        id = Console.ReadLine();
                        trailerR.DeleteTrailer(id);
                        continue;
                }
            }
        }
    }
}
