﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5LiteDB.Model
{
    class Driver
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string pesel { get; set; }
        public int age { get; set; }

        public int truck_Id { get; set; }

        public Driver()
        {
        }

        public Driver(string name, string surname, string pesel, int age, int truck_Id)
        {
            this.name = name;
            this.surname = surname;
            this.pesel = pesel;
            this.age = age;
            this.truck_Id = truck_Id;
        }

        public override string ToString()
        {
            return "Kierowca - Id: " + id + " Imie: " + name + " Nazwisko: " + surname + " Pesel: " + pesel + " Wiek: " + age + (truck_Id == 0 ? " Brak przypisanej ciezarowki" : " Id ciezarowki: " + truck_Id);
        }
    }
}
