﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5LiteDB.Model
{
    class Truck
    {

        public int id { get; set; }
        public string name { get; set; }

        public int trailer_Id { get; set; }

        public Truck()
        {
        }

        public Truck(string name, int trailer_Id)
        {
            this.name = name;
            this.trailer_Id = trailer_Id;
        }

        public override string ToString()
        {
            return "Ciezarowka - Id: " + id + " Nazwa: " + name + (trailer_Id == null ? " Brak przypisanej naczepy " : " Id naczepy: " + trailer_Id);
        }
    }
}
